package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Divination {
	/**
	 * アクセス先URL
	 */
	private String url;

	private String result;

	/**
	 * コンストラクタ
	 * @param url 接続先URL
	 */
	public Divination(String url) {
		this.url = url;
	}

	/**
	 * コンストラクタ
	 */
	public Divination() {
		this("http://api.jugemkey.jp/api/horoscope/free/");
	}

	/**
	 * HTTP接続し、結果をJSON形式のテキストで取得する
	 * @param date 占う日付
	 * @return 占いの結果
	 */
	public String connect(String date) {
		HttpURLConnection  urlConn = null;
		InputStream in = null;
		BufferedReader reader = null;

		try {
			//接続するURLを指定する
			URL url = new URL(this.getUrl() + date);

			//コネクションを取得する
			urlConn = (HttpURLConnection) url.openConnection();

			urlConn.setRequestMethod("GET");

			urlConn.connect();

			int status = urlConn.getResponseCode();

			System.out.println("HTTPステータス:" + status);

			if (status == HttpURLConnection.HTTP_OK) {

				in = urlConn.getInputStream();

				reader = new BufferedReader(new InputStreamReader(in));

				StringBuilder output = new StringBuilder();
				String line;

				while ((line = reader.readLine()) != null) {
					output.append(line);
				}
				this.setResult(output.toString());
				this.setResult(this.getResult().substring(27, this.getResult().length() - 2));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
				if (urlConn != null) {
					urlConn.disconnect();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public String connect() {
		Calendar cl = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		return this.connect(sdf.format(cl.getTime()));
	}

	/**
	 * 占いの結果をランキング順に並べる
	 * @return ランキング順に並んだ占いの結果
	 */
	public List<Map<String, String>> format() {
		Gson gson = new Gson();
		List<Map<String, String>> json = gson.fromJson(this.getResult(), new TypeToken<List<Map<String, String>>>() { }.getType());

		//ランキング順に並び替え
		Map<Integer, Map<String, String>> result = new TreeMap<>();
		for(Map<String, String> map : json) {
			result.put(Integer.parseInt(map.get("rank")), map);
		}



		return new ArrayList<>(result.values());
	}

	/**
	 * @return url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url セットする url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result セットする result
	 */
	public void setResult(String result) {
		this.result = result;
	}
}
