package model;

import java.util.HashMap;
import java.util.Map;

/**
 * ユーザー情報を格納するDB
 * @author Yuhei FUJITA
 *
 */
public class UserTable {
	private Map<String, User> db;

	/**
	 * コンストラクタ
	 */
	public UserTable() {
		this.setDb(new HashMap<>());
	}

	/**
	 * ユーザー追加処理
	 * @param user 追加するユーザーインスタンス
	 * @return 成功すればtrue
	 */
	public boolean insert(User user) {
		if(this.isUseableId(user.getId())) {
			this.getDb().put(user.getId(), user);
			return true;
		}else {
			return false;
		}
	}

	/**
	 * ユーザーの削除
	 * @param id
	 * @param password
	 * @return
	 */
	public boolean delete(String id, String password) {
		if(this.getDb().get(id).isRegularUser(id, password)) {
			this.getDb().remove(id);
			return true;
		}else {
			return false;
		}
	}

	/**
	 * ユーザーIDによる検索
	 * @param id 検索対象ID文字列
	 * @return ヒットしたユーザー一覧
	 */
	public Map<String, User> searchById(String id) {
		//ヒットしたユーザー
		Map<String, User> users = new HashMap<>();

		//検索
		for(String key : this.getDb().keySet()) {
			if(key.contains(id)) {
				users.put(key, this.getDb().get(key));
			}
		}

		return users;
	}

	/**
	 * ユーザー名による検索
	 * @param name 検索対象ユーザー名文字列
	 * @return ヒットしたユーザー一覧
	 */
	public Map<String, User> searchByName(String name) {
		//ヒットしたユーザー
		Map<String, User> users = new HashMap<>();

		//検索
		for(User user : this.getDb().values()) {
			if(user.getName().contains(name)) {
				users.put(user.getId(), user);
			}
		}

		return users;
	}

	/**
	 * IDの重複チェック
	 * @param id 対象のID
	 * @return 重複していなければtrue
	 */
	public boolean isUseableId(String id) {
		if(this.getDb().containsKey(id)) {
			return false;
		}else {
			return true;
		}
	}

	/**
	 * @return db
	 */
	public Map<String, User> getDb() {
		return db;
	}

	/**
	 * @param db セットする db
	 */
	public void setDb(Map<String, User> db) {
		this.db = db;
	}

}
