package model;

import java.util.Date;

public class Post {
	/**
	 * 投稿者ID
	 */
	private User user;
	/**
	 * 投稿内容
	 */
	private String content;
	/**
	 * 投稿時間
	 */
	private Date timeStamp;

	/**
	 * コンストラクタ
	 * @param userId 投稿者ID
	 * @param content 投稿内容
	 */
	public Post(User user, String content) {
		this.user = user;
		this.content = content;
		this.timeStamp = new Date();
	}

	/**
	 * @return content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @return timeStamp
	 */
	public Date getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @return user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user セットする user
	 */
	public void setUser(User user) {
		this.user = user;
	}
}
