package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 単体のユーザー情報を管理するクラス
 * @author Yuhei FUJITA
 */
public class User {
	public static enum Param {
		NAME,
		PASSWORD,
		ZIP_CODE,
		ADDRESS,
		PHONE_NUMBER
	};
	/**
	 * ユーザーID
	 */
	private String id;

	/**
	 * 表示名
	 */
	private String name;

	/**
	 * パスワード
	 */
	private String password;

	/**
	 * 郵便番号
	 */
	private String zipCode;

	/**
	 * 住所
	 */
	private String address;

	/**
	 * 電話番号
	 */
	private String phoneNumber;

	/**
	 * 生年月日
	 */
	private Date birthdate;

	/**
	 * API-KEY
	 */
	private String apiKey;

	/**
	 * コンストラクタ
	 * @param id ID
	 * @param name 名前
	 * @param password パスワード
	 * @param zipCode 郵便番号
	 * @param address 住所
	 * @param phoneNumber 電話番号
	 * @param birthdate 誕生日
	 */
	public User(String id, String name, String password, String zipCode, String address, String phoneNumber, String birthdate) {
		this.setId(id);
		this.setName(name);
		this.setPassword(password);
		this.setZipCode(zipCode);
		this.setAddress(address);
		this.setPhoneNumber(phoneNumber);

		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			this.setBirthdate(format.parse(birthdate));
		} catch (ParseException e) {
			this.setBirthdate(new Date());
			e.printStackTrace();
		}

		String apiKey = HashLogic.toHashValue(id + password + new Date().getTime());
		this.apiKey = apiKey;

		System.out.println(this.toString());
	}

	/**
	 * ユーザー情報のアップデート
	 * @param param 変更する項目
	 * @param newData 新しい値
	 * @return 自分自身（メソッドチェーン）
	 */
	public User update(Param param, String newData) {
		switch(param) {
		case NAME:
			this.setName(newData);
			break;
		case PASSWORD:
			this.setPassword(newData);
			break;
		case ZIP_CODE:
			this.setZipCode(newData);
			break;
		case ADDRESS:
			this.setAddress(newData);
			break;
		case PHONE_NUMBER:
			this.setPhoneNumber(newData);
			break;
		}
		return this;
	}

	/**
	 * 正規のユーザーかどうかのチェック
	 * @param id
	 * @param password
	 * @return
	 */
	public boolean isRegularUser(String id, String password) {
		if(this.getId().equals(id) && this.getPassword().equals(HashLogic.toHashValue(password))) {
			return true;
		}else {
			return false;
		}
	}

	/**
	 * toString()だよ
	 */
	public String toString() {
		String str = "";
		str += "id: " + this.getId() + "\n";
		str += "name: " + this.getName() + "\n";
		str += "password: " + this.getPassword() + "\n";
		str += "zipCode: " + this.getZipCode() + "\n";
		str += "address: " + this.getAddress() + "\n";
		str += "phoneNumber: " + this.getPhoneNumber() + "\n";
		str += "birthdate: " + this.getBirthdate().toString() + "\n";
		str += "api-key: " + this.getApiKey();

		return str;
	}

	/**
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	private void setId(String id) {
		this.id = id;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * パスワードはSHA256でHash化
	 * @param password セットする password
	 */
	public void setPassword(String password) {
		this.password = HashLogic.toHashValue(password);
	}

	/**
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address セットする address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber セットする phoneNumber
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode セットする zipCode
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return birthdate
	 */
	public Date getBirthdate() {
		return birthdate;
	}

	/**
	 * @param birthdate セットする birthdate
	 */
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	/**
	 * @return apiKey
	 */
	public String getApiKey() {
		return apiKey;
	}

	/**
	 * @param apiKey セットする apiKey
	 */
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

}
