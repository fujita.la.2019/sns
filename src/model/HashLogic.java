package model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashLogic {
	/**
	 * コンストラクタ
	 */
	private HashLogic() {}

	public static String toHashValue(String str) {
		return HashLogic.toHashValue(str, "SHA-256");
	}

	public static String toHashValue(String str, String algorithmName) {
		//メッセージダイジェストアルゴリズムの指定
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance(algorithmName);
		}catch(NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		//ハッシュ値をバイト配列で取得
		md.update(str.getBytes());
		byte[] bytes = md.digest();

		//バイト配列を文字列に変換
		StringBuilder sb = new StringBuilder();
		for(byte b : bytes) {
			String hex = String.format("%02x", b);
			sb.append(hex);
		}

		return sb.toString();
	}
}