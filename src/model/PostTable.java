package model;

import java.util.Map;
import java.util.TreeMap;

public class PostTable {
	/**
	 * 投稿テーブル
	 */
	private Map<Long, Post> db;

	/**
	 * コンストラクタ
	 */
	public PostTable() {
		this.setDb(new TreeMap<>());
	}

	/**
	 * 新規投稿の追加
	 * @param post 追加対象の投稿インスタンス
	 * @return 追加できればtrue
	 */
	public boolean insert(Post post) {
		Long key = post.getTimeStamp().getTime();
		if(!this.getDb().containsKey(key)) {
			this.getDb().put(key, post);
			return true;
		}else {
			return false;
		}
	}

	/**
	 * 投稿文による検索
	 * @param content 検索対象文字列
	 * @return ヒットした投稿
	 */
	public Map<Long, Post> searchByContent(String content) {
		Map<Long, Post> result = new TreeMap<>();
		for(Post post : this.getDb().values()) {
			if(post.getContent().contains(content)) {
				result.put(post.getTimeStamp().getTime(), post);
			}
		}
		return result;
	}

	/**
	 * 投稿者IDによる検索
	 * @param userId 検索対象ID（完全一致）
	 * @return ヒットした投稿
	 */
	public Map<Long, Post> searchByUserId(String userId) {
		Map<Long, Post> result = new TreeMap<>();
		for(Post post : this.getDb().values()) {
			if(post.getUser().getId().equals(userId)) {
				result.put(post.getTimeStamp().getTime(), post);
			}
		}
		return result;
	}

	/**
	 * @return db
	 */
	public Map<Long, Post> getDb() {
		return db;
	}

	/**
	 * @param db セットする db
	 */
	public void setDb(Map<Long, Post> db) {
		this.db = db;
	}

}
