package model.api;

import com.google.gson.Gson;

import model.UserTable;

public class Search {
	public static enum TARGET {
		ID,
		NAME,
		POST
	}
	private static final Gson gson = new Gson();

	private Search() {}

	/**
	 * 検索結果をJSON形式のテキストで返す
	 * @param target 検索対象
	 * @param keyword 検索キーワード
	 * @param userTable ユーザーテーブル
	 * @return
	 */
	public static String search(String target, String keyword, UserTable userTable) {
		String result = null;
		if(keyword.equals("")) {
			return "{}";

		}
		switch(target) {
		case "id":
			result = Search.gson.toJson(userTable.searchById(keyword));
			break;
		case "name":
			result = Search.gson.toJson(userTable.searchByName(keyword));
			break;
		default:
			break;
		}
		return result;
	}
}
