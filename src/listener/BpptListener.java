package listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebListener;

import model.PostTable;
import model.User;
import model.UserTable;

/**
 * Application Lifecycle Listener implementation class BpptListener
 *
 */
@WebListener
@MultipartConfig(location="/tmp", maxFileSize=1048576)
public class BpptListener implements ServletContextListener {

	/**
	 * Default constructor.
	 */
	public BpptListener() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0)  {

	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent arg0)  {
		ServletContext app = arg0.getServletContext();

		UserTable userTable = new UserTable();
		User god = new User("god", "神", "Hoge1234", "1800001", "東京都武蔵野市吉祥寺北町1-5-13", "09064199813", "1970/01/01");
		userTable.insert(god);
		app.setAttribute("userTable", userTable);

		PostTable postTable = new PostTable();
		app.setAttribute("postTable", postTable);
	}

}
