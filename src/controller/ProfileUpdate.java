package controller;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;
import model.UserTable;

/**
 * Servlet implementation class ProfileUpdate
 */
@WebServlet("/profile-update")
public class ProfileUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProfileUpdate() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//POSTの値を取得
		String name = request.getParameter("name");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String zipCode = request.getParameter("zipCode");
		String address = request.getParameter("address");
		String phoneNumber = request.getParameter("phoneNumber");

		//アプリケーションスコープ
		ServletContext app = this.getServletContext();
		UserTable userTable = (UserTable)app.getAttribute("userTable");

		//セッションスコープ
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");
		System.out.println(user.toString());
		user.update(User.Param.NAME, name)
		.update(User.Param.PASSWORD, password1)
		.update(User.Param.ZIP_CODE, zipCode)
		.update(User.Param.ADDRESS, address)
		.update(User.Param.PHONE_NUMBER, phoneNumber);

		response.sendRedirect("/sns/profile?id=" + user.getId());
	}

}
