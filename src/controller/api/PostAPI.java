package controller.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import model.Post;
import model.PostTable;
import model.User;

/**
 * Servlet implementation class PostAPI
 */
@WebServlet("/api/post")
public class PostAPI extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PostAPI() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//投稿一覧を取得
		ServletContext app = this.getServletContext();
		PostTable postTable = (PostTable) app.getAttribute("postTable");

		//JSONにパース
		Gson gson = new Gson();
		String json = gson.toJson(postTable.getDb());

		//レスポンス生成
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		response.getWriter().println(json);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//requestからJSONを取り出す
		BufferedReader br = new BufferedReader( request.getReader() );
		String requestJson = br.readLine();

		//JSONをMapにパース
		Gson gson = new Gson();
		Map<String, String> jsonMap = gson.fromJson(requestJson, Map.class);

		//投稿一覧を取得
		ServletContext app = this.getServletContext();
		PostTable postTable = (PostTable) app.getAttribute("postTable");

		//ユーザー情報を取得
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");

		postTable.insert(new Post(user, jsonMap.get("content")));
	}

}
