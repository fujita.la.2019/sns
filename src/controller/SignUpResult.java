package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;
import model.UserTable;

/**
 * Servlet implementation class SignUpResult
 */
@WebServlet("/sign-up-result")
public class SignUpResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUpResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//POSTの値を取得
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String zipCode = request.getParameter("zipCode");
		String address = request.getParameter("address");
		String phoneNumber = request.getParameter("phoneNumber");
		String birthdate = request.getParameter("birthdate");

		//アプリケーションスコープ
		ServletContext app = this.getServletContext();
		UserTable userTable = (UserTable)app.getAttribute("userTable");

		//有効なIDか確認
		if(userTable.isUseableId(id) && password1.equals(password2)) {
			//ユーザー生成
			User user = new User(id, name, password1, zipCode, address, phoneNumber, birthdate);
			userTable.insert(user);

			//セッションスコープ
			HttpSession session = request.getSession();
			session.setAttribute("user", user);

			//ホームに戻す
			response.sendRedirect("/sns/home");
		}else {
			List<String> signUpError = new ArrayList<>();
			if(!userTable.isUseableId(id)) {
				signUpError.add("そのIDは既に使用されています");
			}
			if(!password1.equals(password2)) {
				signUpError.add("パスワードが同じではありません");
			}
			HttpSession session = request.getSession();
			session.setAttribute("signUpError", signUpError);
			//戻す
			response.sendRedirect("/sns/sign-up");
		}
	}

}
