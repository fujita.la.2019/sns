<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/common/head.jsp"%>
<title>Home</title>
</head>
<body>
	<%@ include file="/common/nav.jsp"%>
	<div class="row">
		<div class="col s12 m10 offset-m1 l6 offset-l3">
			<h1>今日の運勢</h1>
			<ul class="collapsible popout">
				<c:forEach var="item" items="${ divination }">
					<li>
						<div class="collapsible-header">
							第<c:out value="${ item.rank }" />位：<c:out value="${ item.sign }" />
						</div>
						<div class="collapsible-body">
							<table>
								<tr>
									<td>コメント</td>
									<td><c:out value="${ item.content }" /></td>
								</tr>
								<tr>
									<td>ラッキーアイテム</td>
									<td><c:out value="${ item.item }" /></td>
								</tr>
								<tr>
									<td>ラッキーカラー</td>
									<td><c:out value="${ item.color }" /></td>
								</tr>
								<tr>
									<td>金運</td>
									<td><c:out value="${ item.money }" /></td>
								</tr>
								<tr>
									<td>仕事運</td>
									<td><c:out value="${ item.job }" /></td>
								</tr>
								<tr>
									<td>恋愛運</td>
									<td><c:out value="${ item.love }" /></td>
								</tr>
								<tr>
									<td>総合運</td>
									<td><c:out value="${ item.total }" /></td>
								</tr>
							</table>
						</div>
					</li>
				</c:forEach>
			</ul>
		</div>
	</div>
	<jsp:include page="/common/script.jsp"></jsp:include>
</body>
</html>