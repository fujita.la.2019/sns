<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/common/head.jsp"%>
<title>Home</title>
</head>
<body>
	<div id="app">
		<%@ include file="/common/nav.jsp"%>
		<div class="row">
			<div class="col s12 m10 offset-m1 l6 offset-l3">
				<c:if test="${ user != null }">
					<div class="row card-panel">
						<div class="progress" v-if="progress">
							<div class="indeterminate"></div>
						</div>
						<div class="input-field col s12">
							<textarea id="textarea" class="materialize-textarea"
								v-model="content"></textarea>
							<label for="textarea">投稿内容</label>
						</div>
						<button class="btn waves-effect waves-light right" type="submit"
							name="action" @click="submit">
							送信<i class="material-icons right">send</i>
						</button>
					</div>
				</c:if>
				<ul class="collapsible popout">
					<ul class="collapsible popout">
						<li v-for="post in posts.data">
							<div class="collapsible-header">【{{ post.user.name }}】 {{
								format(post.timeStamp) }}</div>
							<div class="collapsible-body">
								<pre>{{ post.content }}</pre>
							</div>
						</li>
					</ul>
				</ul>
			</div>
		</div>
	</div>
	<jsp:include page="/common/script.jsp"></jsp:include>
	<script
		src="https://cdn.jsdelivr.net/npm/axios@0.12.0/dist/axios.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/lodash@4.13.1/lodash.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>
	<script>
		var app = new Vue({
			el: '#app',
			data: {
				content: '',
				progress: false,
				posts: '',
			},
			mounted: function() {
				axios.get('/sns/api/post')
				.then(function(response) {
					this.posts = response
					M.AutoInit()
				}.bind(this))
			},
			methods: {
				submit: function() {
					this.progress = true
					axios.post('/sns/api/post', {
						content: this.content
					})
					.then(function(response) {
						this.result = response
						this.content = ''
						M.toast({html: '投稿しました'})
					}.bind(this))
					.catch(function(response) {
						M.toast({html: '投稿できませんでした'})
					}.bind(this))
					.	finally(function() {
						this.progress = false
						M.AutoInit()
					}.bind(this))

					axios.get('/sns/api/post')
					.then(function(response) {
						this.posts = response
						M.AutoInit()
					}.bind(this))
				},
				updatePosts: function() {
					axios.get('')
				},
				format: function(str) {
					let date = new Date(str)
					return moment(date).format("YYYY/MM/DD LT")
				}
			}
		});
	</script>
</body>
</html>