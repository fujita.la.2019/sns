<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/common/head.jsp"%>
<title>検索</title>
</head>
<body>
	<div id="app">
		<%@ include file="/common/nav.jsp"%>
		<div class="row">
			<div class="col s12 m10 offset-m1 l6 offset-l3">
				<h1>検索</h1>
				<div class="card-panel">
					<div class="row">
						<div class="col s2">
							<label> <input name="target" type="radio" value="id"
								checked v-model="target" /> <span>id</span>
							</label>
						</div>
						<div class="col s2">
							<label> <input name="target" type="radio" value="name"
								v-model="target" /> <span>名前</span>
							</label>
						</div>
						<div class="col s2">
							<label> <input name="target" type="radio" value="post" disabled
								v-model="target" /> <span>投稿</span>
							</label>
						</div>
					</div>
					<div class="input-field">
						<input id="keyword" type="text" class="validate" v-model="keyword">
						<label for="keyword">検索ワード</label>
					</div>
				</div>
				<div class="row">
					<div class="col s12 center">
						<div class="preloader-wrapper big active" v-show="progress">
							<div class="spinner-layer spinner-blue-only">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="gap-patch">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<ul class="collapsible popout">
					<li v-for="user in result.data" v-key="user.id">
						<div class="collapsible-header">{{ user.name }}</div>
						<div class="collapsible-body">
							<table>
								<tr>
									<td>ID</td>
									<td>{{ user.id }}</td>
								</tr>
								<tr>
									<td>名前</td>
									<td>{{ user.name }}</td>
								</tr>
								<tr>
									<td>郵便番号</td>
									<td>{{ user.zipCode }}</td>
								</tr>
								<tr>
									<td>住所</td>
									<td>{{ user.address }}</td>
								</tr>
								<tr>
									<td>電話番号</td>
									<td>{{ user.phoneNumber }}</td>
								</tr>
								<tr>
									<td>パスワード（ハッシュ値）</td>
									<td>{{ user.password }}</td>
								</tr>
							</table>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<jsp:include page="/common/script.jsp"></jsp:include>
	<script
		src="https://cdn.jsdelivr.net/npm/axios@0.12.0/dist/axios.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/lodash@4.13.1/lodash.min.js"></script>
	<script>
		var app = new Vue({
			el: "#app",
			data: {
				target: "id",
				keyword: '',
				result: {},
				progress: false,
			},
			watch: {
				target: function() {
					this.progress = true
					this.search()
				},
				keyword: function() {
					this.progress = true
					this.debouncedGetAnswer()
				},
			},
			created: function() {
				this.debouncedGetAnswer = _.debounce(this.search, 1000)
			},
			methods: {
				search: function() {
					axios.get('/sns/api/search?target=' + this.target + '&keyword=' + this.keyword)
					.then(function(response) {
						this.result = response
					}.bind(this))
					.catch(function(response) {
						this.result = 'error'
					}.bind(this))
					.	finally(function() {
						this.progress = false
						M.AutoInit()
					}.bind(this))
				},
			}
		});
	</script>
</body>
</html>