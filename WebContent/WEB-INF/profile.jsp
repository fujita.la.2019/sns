<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>プロファイル</title>
<%@ include file="/common/head.jsp"%>
</head>
<body>
	<%@ include file="/common/nav.jsp"%>
	<div class="row">
		<div class="col s12 m10 offset-m1 l6 offset-l3">
			<h1>プロファイル</h1>
			<c:if test="${param.id == user.id }" var="flag" />
			<c:if test="${ flag }">
				<form action="/sns/profile-update" method="post">
					<div class="row">
						<div class="input-field col s12">
							<input id="id" type="text" name="id" class="validate"
								pattern="^[a-zA-Z][a-zA-Z0-9]{0,7}$" required value="${ user.id }" disabled> <label
								for="id">ID</label>
						</div>
						<div class="input-field col s12">
							<input id="password1" type="password" name="password1"
								class="validate" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
								required> <label for="password1">パスワード</label>
						</div>
						<div class="input-field col s12">
							<input id="password2" type="password" name="password2"
								class="validate" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
								required> <label for="password2">パスワード（再入力）</label>
						</div>
						<div class="input-field col s12">
							<input id="name" type="text" name="name" class="validate"
								required  value="${ user.name }"> <label for="name">名前</label>
						</div>
						<div class="input-field col s12">
							<input id="zipCode" type="text" name="zipCode" class="validate"
								pattern="^[0-9]{7}$" required  value="${ user.zipCode }"
								onKeyUp="AjaxZip3.zip2addr(this,'','address','address');">
							<label for="zipCode">郵便番号</label>
						</div>
						<div class="input-field col s12">
							<input id="address" type="text" name="address" class="validate"
								required value="${ user.address }"> <label for="address">住所</label>
						</div>
						<div class="input-field col s12">
							<input id="phoneNumber" type="text" name="phoneNumber"
								class="validate" pattern="^[0-9]{11}$" required value="${ user.phoneNumber }"> <label
								for="phoneNumber">電話番号</label>
						</div>
					</div>
					<button class="btn waves-effect waves-light" type="submit"
						name="action">
						更新<i class="material-icons right"></i>
					</button>
				</form>
			</c:if>
			<c:if test="${ !flag }">
				<table>
					<tr>
						<td>ID</td>
						<td>${ userTable.db[param.id].id }</td>
					</tr>
					<tr>
						<td>名前</td>
						<td>${ userTable.db[param.id].name }</td>
					</tr>
				</table>
			</c:if>
		</div>
	</div>
	<script src="https://ajaxzip3.github.io/ajaxzip3.js"></script>
	<jsp:include page="/common/script.jsp"></jsp:include>
</body>
</html>