<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>サインイン</title>
<%@ include file="/common/head.jsp"%>
</head>
<body>
	<%@ include file="/common/nav.jsp"%>
	<div id="app" class="row">
		<div class="col s12 m10 offset-m1 l6 offset-l3">
			<h1>サインイン</h1>
			<form action="/sns/sign-in-result" method="post">
				<div class="row">
					<div class="input-field col s12">
						<input id="id" type="text" name="id" class="validate"
							pattern="^[a-zA-Z][a-zA-Z0-9]{0,7}$" required> <label
							for="id">ID</label>
					</div>
					<div class="input-field col s12">
						<input id="password1" type="password" name="password"
							class="validate" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
							required> <label for="password1">パスワード</label>
					</div>
				</div>
				<button class="btn waves-effect waves-light" type="submit"
					name="action">
					ログイン<i class="material-icons right"></i>
				</button>
			</form>
		</div>
	</div>
	<jsp:include page="/common/script.jsp"></jsp:include>
</body>
</html>