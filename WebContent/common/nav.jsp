<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="navbar-fixed">
	<nav>
		<div class="nav-wrapper">
			<a href="/sns/home" class="brand-logo center">SNS</a>
			<ul class="right hide-on-med-and-down">
				<li><a href="/sns/search">検索</a></li>
				<c:if test="${ user != null }">
					<li><a href="/sns/profile?id=${ user.id }">プロファイル</a></li>
				</c:if>
				<c:if test="${ user == null }">
					<li><a href="/sns/sign-in">サインイン</a></li>
					<li><a href="/sns/sign-up">サインアップ</a></li>
				</c:if>
				<c:if test="${ user != null }">
					<li><a href="/sns/sign-out">サインアウト</a></li>
				</c:if>
			</ul>
		</div>
	</nav>
</div>